#!/bin/bash

set -e # Exit if a command has an error
set -u # Triggers an error whenever an undefined variable is used

## User-defined variables, in Pipelines Settings
# SSH_HOST: deployment host
# DEPLOY_CUSTOMER
# DEPLOY_PROJECT
# DEPLOY_URL_OVERRIDE: optional, overrides autogenerated DEPLOY_URL
# SLACK_WEBHOOK: URL given in slack integrations
# SLACK_CHANNEL: optional, defaults to "#dev"

## Default Bitbucket Pipelines variables
# BITBUCKET_BRANCH

if echo "$BITBUCKET_BRANCH" | grep '/' > /dev/null; then
  echo "Exiting beacuse branch not allowed to deploy"
  exit 0
fi

DEPLOYED_URL="$DEPLOY_PROJECT.$DEPLOY_CUSTOMER.$SSH_HOST"
DEPLOYED_URL="${DEPLOY_URL_OVERRIDE:-$DEPLOYED_URL}"
if [ "$BITBUCKET_BRANCH" != "master" ]; then
  DEPLOYED_URL="$BITBUCKET_BRANCH.$DEPLOYED_URL"
fi

TEXT="Deployed branch *$BITBUCKET_BRANCH* in <http://$DEPLOYED_URL/|$DEPLOYED_URL>"
CHANNEL="${SLACK_CHANNEL:-#dev}"
JSON='{"channel":"'$CHANNEL'","icon_emoji":":rocket:","color":"good","text":"'$TEXT'"}'

curl -X POST -H 'Content-type: application/json' --data "$JSON" "$SLACK_WEBHOOK"
