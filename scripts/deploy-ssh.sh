#!/bin/bash

set -e # Exit if a command has an error
set -u # Triggers an error whenever an undefined variable is used

## User-defined variables, in Pipelines Settings
# DEPLOY_CUSTOMER
# DEPLOY_PROJECT
# DEPLOY_PATH: optional, can't contain ".."

## Default Bitbucket Pipelines variables
# BITBUCKET_BRANCH
# BITBUCKET_REPO_SLUG

if [ -z "${SSH_HOST:-}" ] \
|| [ -z "${SSH_USER:-}" ] \
|| [ -z "${SSH_KEY:-}" ] \
|| [ -z "${SSH_HOST_FINGERPRINT:-}" ] \
|| [ -z "${DEPLOY_CUSTOMER:-}" ] \
|| [ -z "${DEPLOY_PROJECT:-}" ]
then
  echo "ERROR: Some user-defined variables are not defined!" >&2
  exit 1
fi

if [ "${1:-}" == "--check-env" ]; then
  echo "OK: user-defined variables are correctly filled."
  exit 0
fi

if echo "$BITBUCKET_BRANCH" | grep '/' > /dev/null; then
  echo "Exiting because branch not allowed to deploy"
  exit 0
fi

DIST_DIR="${1%/}"

DEPLOY_DEFAULT_PATH="/www/$DEPLOY_CUSTOMER.$DEPLOY_PROJECT/$BITBUCKET_BRANCH/"
DEPLOY_PATH="${DEPLOY_PATH:-$DEPLOY_DEFAULT_PATH}"

if echo "$DEPLOY_PATH" | grep '\.\.' > /dev/null; then
  echo "Error: Wrong DEPLOY_PATH"
  exit 1
fi

if [ ! -f ~/.ssh/id_rsa ]; then
  bash "$(dirname ${BASH_SOURCE[0]})/setup-ssh.sh"
fi

SSH_CONN="$SSH_USER@$SSH_HOST"

ssh -oBatchMode=yes -i ~/.ssh/id_rsa "$SSH_CONN" "mkdir -p $DEPLOY_PATH" # shellcheck disable=SC2029 : variable is expanded client-side
scp -oBatchMode=yes -i ~/.ssh/id_rsa -r "$DIST_DIR"/* "$SSH_CONN:$DEPLOY_PATH"
