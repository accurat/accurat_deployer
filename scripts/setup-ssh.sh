#!/bin/bash

set -e # Exit if a command has an error
set -u # Triggers an error whenever an undefined variable is used

## User-defined variables, in Pipelines Settings
# SSH_HOST: deployment host
# SSH_USER: ssh user
# SSH_KEY: get it from `cat ~/.ssh/deploy_key_id_rsa | base64 --wrap=0`
# SSH_HOST_FINGERPRINT: get it from `ssh-keyscan -t rsa "$SSH_HOST"`

if [ -z "${SSH_HOST:-}" ] \
|| [ -z "${SSH_USER:-}" ] \
|| [ -z "${SSH_KEY:-}" ] \
|| [ -z "${SSH_HOST_FINGERPRINT:-}" ]
then
  echo "ERROR: Some user-defined variables are not defined!" >&2
  exit 1
fi

if [ "${1:-}" == "--check-env" ]; then
  echo "OK: user-defined variables are correctly filled."
  exit 0
fi

mkdir -p ~/.ssh
echo "$SSH_HOST_FINGERPRINT" >> ~/.ssh/known_hosts
(umask 077; echo "$SSH_KEY" | base64 --decode > ~/.ssh/id_rsa)
